package xivo.replication.jobs

import java.security.InvalidParameterException
import java.sql.Connection

import org.slf4j.LoggerFactory
import xivo.replication.sqlUtils.{TableUtils, Table, Replicator}
import xivo.replication.config.Conf

class DeltaReplicator(table: Table) extends Replicator(table: Table) {

  if(table.referenceColumn.isEmpty) throw new InvalidParameterException(s"No reference column for table $table")
  var tableUtils = new TableUtils(table, Conf.Replication)

  override val logger = LoggerFactory.getLogger(getClass)
  var lastId = 0
  var newLastId = 0
  var logCounter = 0

  def replicate(reportingConnection: Connection, sourceConnection: Connection, maxId: Int) = {
    lastId = tableUtils.getReferenceId()(sourceConnection)
    newLastId = lastId
    logger.debug(s"Starting replication of table ${table.name}, starting with id $lastId.")
    logger.debug(s"The replicated columns are ${table.columns}.")
    val selectStmt = sourceConnection.prepareStatement(createSelectStatement(maxId))
    selectStmt.setFetchSize(1000)
    val insertStmt = reportingConnection.prepareStatement(createInsertStatement())
    val rs = selectStmt.executeQuery()
    while (rs.next()) {
      for (i <- 1 to getTableColumnSize(table)) {
        insertStmt.setObject(i, rs.getObject(i))
      }
      newLastId += insertStmt.executeUpdate()
    }
    rs.close()
    tableUtils.setReferenceId(newLastId)(sourceConnection)
    logReplicateActivity(lastId, newLastId)
    logger.debug(s"Replication for table ${table.name} finished, the new reference id is $newLastId.")
  }

  def createSelectStatement(maxId: Int): String = {
    val refColumn = table.referenceColumn.get
    super.createSelectStatement() + s""" WHERE "$refColumn" > $lastId AND "$refColumn" <= $maxId ORDER BY "$refColumn" ASC"""
  }

  def getTableColumnSize(table: Table): Int = {
    table.referenceServerGenerated match {
      case Some(true) => table.columns.size -1
      case _ => table.columns.size
    }
  }

  def logReplicateActivity(before : Int, after : Int) : Unit = {
    logCounter = logCounter + (after - before)
    if (logCounter > Conf.Replication.getDeltaLogThreshold()) {
      logger.info(s"Replication of $logCounter rows in ${table.name} since last time, last local id is $after.")
      logCounter = 0
    }
  }

}
