package xivo.replication.sqlUtils

import java.sql.Connection

import org.scalatest.mock.EasyMockSugar
import org.scalatest.{FlatSpec, Matchers}

class ConnectionProviderSpec extends FlatSpec with Matchers with EasyMockSugar {

  "A ConnectionProvider" should "replace invalid connections with new ones" in {
    val f1 = mock[ConnectionFactory]
    val f2 = mock[ConnectionFactory]
    val (c11, c12, c21, c22) = (mock[Connection], mock[Connection], mock[Connection], mock[Connection])
    f1.getConnection.andReturn(c11)
    f2.getConnection.andReturn(c21)

    c11.isValid(1).andReturn(false).anyTimes()
    f1.uri.andReturn("uri://test").anyTimes()
    f1.getConnection.andThrow(new RuntimeException("Test exception"))
    f1.getConnection.andReturn(c12)
    c12.isValid(1).andReturn(true)
    c21.isValid(1).andReturn(false)
    f2.uri.andReturn("uri://test2")
    f2.getConnection.andReturn(c22)
    c22.isValid(1).andReturn(true)

    whenExecuting(f1, f2, c11, c12, c21, c22) {
      val provider = new ConnectionProvider(f1, f2)
      provider.getReportingConnection shouldBe theSameInstanceAs(c12)
      provider.getSourceConnection shouldBe theSameInstanceAs(c22)
    }

  }
}
