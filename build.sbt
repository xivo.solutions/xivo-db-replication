import AssemblyKeys._
import com.typesafe.sbt.packager.docker._
import NativePackagerHelper._
import org.clapper.sbt.editsource.EditSourcePlugin.autoImport._
import org.clapper.sbt.editsource.EditSourcePlugin

name := "xivo-db-replication"

val appVersion:String = sys.env.get("TARGET_VERSION").getOrElse("2017.03.01")

version := appVersion

lazy val root = (project in file(".")).enablePlugins(DockerPlugin).enablePlugins(JavaServerAppPackaging)

scalaVersion := "2.11.7"

resolvers += "softprops-maven" at "http://dl.bintray.com/content/softprops/maven"

libraryDependencies ++= Seq("commons-configuration" % "commons-configuration" % "1.7",
                            "org.postgresql" % "postgresql" % "42.2.4",
                            "ch.qos.logback" % "logback-classic" % "1.0.7",
                            "com.typesafe.akka" %% "akka-actor" % "2.3.11",
                            "com.typesafe" % "config" % "1.3.0",
                            "joda-time" % "joda-time" % "2.0",
                            "com.typesafe.play" %% "anorm" % "2.4.0",
                            "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.6.1",
                            "com.fasterxml.jackson.core" % "jackson-databind" % "2.6.1",
                            "com.iheart" %% "ficus" % "1.4.3",
                            "org.dbunit" % "dbunit" % "2.4.7" % "test",
                            "org.scalatest" %% "scalatest" % "2.2.4" % "test",
                            "org.easymock" % "easymock" % "3.1" % "test",
                            "org.mockito" % "mockito-all" % "1.8.4" % "test",
                            "com.typesafe.akka" %% "akka-testkit" % "2.3.11" % "test",
                            "org.specs2" %% "specs2-core" % "3.6.4" % "test",
                            "org.scalikejdbc" %% "scalikejdbc" % "2.1.1" % "test",
                            "org.dbunit" % "dbunit" % "2.4.7" % "test")

mainClass in Compile := Some("xivo.replication.Main")

assemblySettings

mergeStrategy in assembly <<= (mergeStrategy in assembly) {
  (old) => {
    case PathList("org", "apache", "commons", xs@_*) => MergeStrategy.first
    case x => old(x)
  }
}

test in assembly := {}

dockerCommands += Cmd("ENV", "DB_NAME xivo_stats")

dockerCommands += Cmd("ENV", "DB_PORT 5443")

dockerCommands += Cmd("ENV", "DB_USERNAME asterisk")

dockerCommands += Cmd("ENV", "DB_PASSWORD proformatique")

dockerCommands += Cmd("ADD", "./opt/docker/sqlscripts/ $LIQUIBASE_HOME/sqlscripts")

dockerCommands += Cmd("LABEL", s"""version="${appVersion}"""")

maintainer in Docker := "R&D <randd@xivo.solutions>"

dockerBaseImage := "xivoxc/javaliquibase:1.5.0"

dockerRepository := Some("xivoxc")

dockerExposedVolumes := Seq("/var/log")

dockerEntrypoint := Seq("/usr/local/bin/start.sh","/opt/docker/bin/xivo-db-replication-docker")

val setVersionVarTask = Def.task {
    System.setProperty("XIVO_REPLIC_VERSION", appVersion)
}

edit in EditSource <<= (edit in EditSource) dependsOn (EditSourcePlugin.autoImport.clean in EditSource)

packageBin in Compile <<= (packageBin in Compile) dependsOn (edit in EditSource)

run in Compile <<= (run in Compile) dependsOn setVersionVarTask

flatten in EditSource := true

targetDirectory in EditSource <<= baseDirectory / "conf"

variables in EditSource <+= version {version => ("SBT_EDIT_APP_VERSION", appVersion)}

(sources in EditSource) <++= baseDirectory map { bd =>
    (bd / "src/res" * "appli.version").get
}

mappings in Universal += file("conf/appli.version") -> "conf/appli.version"

javaOptions in Test += "-Dconfig.file=src/test/resources/application.conf"
