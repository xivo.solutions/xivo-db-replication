# xivo-db-replication

Database replication program for XiVO

## Adding replication for a table

If you want to add replication for a table from XiVO, you need to:

1. Add the table name in the reference.conf file
1. Add a section in reference.conf file with the columns you want to replicate and the replication mode (DELTA or CLEAN_INSERT)
1. Add a liquibase changeSet in the db.changelog.xml file with its associated sql file that creates the new table: it will
   create the new table in the xivo_stat database

### Note on readonly user `stats`

A readonly user 'dbreplic' is created for the xivo_stats database.
It has, by default, select rights on TABLE and SEQUENCE in the public SCHEMA of the source database (see last sql migration).

## How to develop :

Build and dependencies are managed with SBT.

## Testing

**Prerequistes:** a local PostgreSQL instance, with a database asterisktest owned by user asterisk (password: asterisk).
Table callback_request owned by asterisk must exist. See src/universal/sqlscripts.

    $ sbt test

## Build Docker image

    $ sbt docker:publishLocal

## Configuration

### Environment variables

The following environment variables may be used:
- **DB_HOST:** the IP address or FQDN of source database to replicate
- **REPORTING_DB_HOST:** the IP address or FQDN of reporting database

### Replication settings

A table can be added to the replication by adding the following configuration:
<pre>
replication.new_table_name {
  columns = ["ref_column","column1","column2"]
  replicationType = DELTA
  sourceDatabase = someDatabase
  referenceColumn = ref_column
}
</pre>

Also do not forget to add this table to `replication.tables` and to define the eventual new database.
Please note that the new table schema must be created via a liquibase script.


## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the [COPYING](COPYING) and [COPYING.LESSER](COPYING.LESSER) files for details.

## Documentation

    http://xivocc.readthedocs.org
