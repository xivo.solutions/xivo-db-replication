CREATE TABLE public.qualifications (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    active integer DEFAULT 1 NOT NULL
);

CREATE TABLE public.subqualifications (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    qualification_id integer NOT NULL,
    active integer DEFAULT 1 NOT NULL
);

CREATE TABLE public.qualification_answers (
    id integer NOT NULL,
    sub_qualification_id integer,
    "time" timestamp without time zone,
    callid character varying(128) NOT NULL,
    agent integer,
    queue integer,
    first_name character varying(128),
    last_name character varying(128),
    comment character varying(255),
    custom_data text
);
