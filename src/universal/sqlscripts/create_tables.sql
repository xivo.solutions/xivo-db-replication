DROP TABLE IF EXISTS agentgroup;
CREATE TABLE agentgroup (
    id SERIAL PRIMARY KEY, 
    groupid INTEGER NOT NULL,
    name VARCHAR(128) NOT NULL DEFAULT '',
    groups VARCHAR(255) DEFAULT '', 
    commented INTEGER NOT NULL DEFAULT 0,
    deleted INTEGER NOT NULL DEFAULT 0,
    description TEXT
);

DROP TABLE IF EXISTS agentfeatures;
CREATE TABLE agentfeatures(
    id integer PRIMARY KEY,
    numgroup INTEGER NOT NULL DEFAULT 0,
    number VARCHAR(40),
    firstname VARCHAR(128),
    lastname VARCHAR(128)
);

DROP TABLE IF EXISTS queuefeatures;
CREATE TABLE queuefeatures (
    id integer PRIMARY KEY,
    name VARCHAR(128),
    displayname VARCHAR(128),
    number VARCHAR(40)
);


DROP TABLE IF EXISTS "cel" CASCADE;
CREATE TABLE "cel" (
 "id" serial ,
 "eventtype" VARCHAR (30) NOT NULL ,
 "eventtime" timestamp NOT NULL ,
 "userdeftype" VARCHAR(255) NOT NULL ,
 "cid_name" VARCHAR (80) NOT NULL ,
 "cid_num" VARCHAR (80) NOT NULL ,
 "cid_ani" VARCHAR (80) NOT NULL ,
 "cid_rdnis" VARCHAR (80) NOT NULL ,
 "cid_dnid" VARCHAR (80) NOT NULL ,
 "exten" VARCHAR (80) NOT NULL ,
 "context" VARCHAR (80) NOT NULL ,
 "channame" VARCHAR (80) NOT NULL ,
 "appname" VARCHAR (80) NOT NULL ,
 "appdata" VARCHAR (512) NOT NULL ,
 "amaflags" int NOT NULL ,
 "accountcode" VARCHAR (20) NOT NULL ,
 "peeraccount" VARCHAR (20) NOT NULL ,
 "uniqueid" VARCHAR (150) NOT NULL ,
 "linkedid" VARCHAR (150) NOT NULL ,
 "userfield" VARCHAR (255) NOT NULL ,
 "peer" VARCHAR (80) NOT NULL ,
 "call_log_id" INTEGER DEFAULT NULL,
 PRIMARY KEY("id")
);

CREATE INDEX "cel__idx__linkedid" ON "cel"("linkedid");
CREATE INDEX "cel__idx__eventtime" ON "cel"("eventtime");
CREATE INDEX "cel__idx__uniqueid" ON "cel"("uniqueid");


DROP TABLE IF EXISTS "queue_log" ;
CREATE TABLE "queue_log" (
 "time" VARCHAR(26) DEFAULT ''::VARCHAR NOT NULL,
 "callid" VARCHAR(32) DEFAULT ''::VARCHAR NOT NULL,
 "queuename" VARCHAR(50) DEFAULT ''::VARCHAR NOT NULL,
 "agent" VARCHAR(50) DEFAULT ''::VARCHAR NOT NULL,
 "event" VARCHAR(20) DEFAULT ''::VARCHAR NOT NULL,
 "data1" TEXT DEFAULT ''::TEXT,
 "data2" TEXT DEFAULT ''::TEXT,
 "data3" TEXT DEFAULT ''::TEXT,
 "data4" TEXT DEFAULT ''::TEXT,
 "data5" TEXT DEFAULT ''::TEXT,
 "id" SERIAL PRIMARY KEY
 );

CREATE INDEX queue_log__idx_time ON queue_log USING btree ("time");
CREATE INDEX queue_log__idx_callid ON queue_log USING btree ("callid");
CREATE INDEX queue_log__idx_queuename ON queue_log USING btree ("queuename");
CREATE INDEX queue_log__idx_event ON queue_log USING btree ("event");
CREATE INDEX queue_log__idx_agent ON queue_log USING btree ("agent");

DROP TABLE IF EXISTS "dialaction";
CREATE TABLE "dialaction" (
 "event" VARCHAR(80) NOT NULL,
 "category" VARCHAR(80),
 "categoryval" VARCHAR(128) NOT NULL DEFAULT '',
 "action" VARCHAR(80) NOT NULL,
 "actionarg1" VARCHAR(255) DEFAULT NULL::character varying,
 "actionarg2" VARCHAR(255) DEFAULT NULL::character varying,
 "linked" INTEGER NOT NULL DEFAULT 0, -- BOOLEAN
 PRIMARY KEY("event","category","categoryval")
);

CREATE INDEX "dialaction__idx__action_actionarg1" ON "dialaction"("action","actionarg1");

DROP TABLE IF EXISTS "extensions";
CREATE TABLE "extensions" (
 "id" SERIAL,
 "commented" INTEGER NOT NULL DEFAULT 0, -- BOOLEAN
 "context" VARCHAR(39) NOT NULL DEFAULT '',
 "exten" VARCHAR(40) NOT NULL DEFAULT '',
 "type" VARCHAR(80) NOT NULL,
 "typeval" VARCHAR(255) NOT NULL DEFAULT '',
 PRIMARY KEY("id")
);

DROP TABLE IF EXISTS "state";
CREATE TABLE "state" (
 "name" VARCHAR(128) NOT NULL,
 "val" INTEGER NOT NULL,
 PRIMARY KEY("name")
);

INSERT INTO state(name, val) VALUES('cel.replication.lastId', 0);
INSERT INTO state(name, val) VALUES('queue_log.replication.lastId', 0);

GRANT SELECT ON ALL TABLES IN SCHEMA public TO stats;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public TO stats;
