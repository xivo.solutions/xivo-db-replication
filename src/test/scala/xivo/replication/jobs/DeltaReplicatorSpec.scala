package xivo.replication.jobs

import java.security.InvalidParameterException
import java.sql.{Connection, PreparedStatement, ResultSet}

import org.easymock.EasyMock
import org.scalatest.mock.EasyMockSugar
import org.scalatest.mock.{MockitoSugar => mockito}
import org.mockito.Matchers.anyString
import org.mockito.Mockito.{never,verify}
import org.scalatest.{FlatSpec, Matchers}
import org.slf4j.Logger
import xivo.replication.sqlUtils.{Table, TableUtils}

class DeltaReplicatorSpec extends FlatSpec with Matchers with EasyMockSugar {

  "A DeltaReplicator" should "generate a SELECT query with a WHERE and a ORDER BY clause" in {
    val table = Table("cel", List("id", "uniqueid", "linkedid", "the_ref"), "xivo", Some("the_ref"))
    val lastId = 12356
    val replicator = new DeltaReplicator(table)
    replicator.lastId = lastId
    val maxId = 13589

    replicator.createSelectStatement(maxId) shouldEqual
      s"""SELECT "id", "uniqueid", "linkedid", "the_ref" FROM cel WHERE "the_ref" > $lastId AND "the_ref" <= $maxId ORDER BY "the_ref" ASC"""
  }

  it should "replicate the data" in {
    val table = Table("cel", List("reference_number", "column1", "column2", "column3"), "xivo", Some("reference_number"), Some(true))
    val reporting = niceMock[Connection]
    implicit val source: Connection = niceMock[Connection]
    val selectStmt = mock[PreparedStatement]
    val insertStmt = mock[PreparedStatement]
    val rs = mock[ResultSet]
    val lastId = 49000
    val tableUtils = mock[TableUtils]

    tableUtils.getReferenceId().andReturn(lastId)
    source.prepareStatement(EasyMock.anyObject(classOf[String])).andReturn(selectStmt)
    selectStmt.setFetchSize(1000)
    reporting.prepareStatement(EasyMock.anyObject(classOf[String])).andReturn(insertStmt)
    selectStmt.executeQuery().andReturn(rs)
    rs.next().andReturn(true)
    rs.getObject(1).andReturn("a")
    insertStmt.setObject(1, "a")
    rs.getObject(2).andReturn("b")
    insertStmt.setObject(2, "b")
    rs.getObject(3).andReturn("c")
    insertStmt.setObject(3, "c")
    insertStmt.executeUpdate().andReturn(12)
    rs.next().andReturn(false)
    rs.close()
    tableUtils.setReferenceId(49012)

    whenExecuting(reporting, source, selectStmt, insertStmt, rs, tableUtils) {
      val replicator = new DeltaReplicator(table)
      replicator.tableUtils = tableUtils
      replicator.replicate(reporting, source, 50)
      replicator.newLastId shouldEqual 49012
    }
  }

  it should "throw an exception at instanciation if the table does not have a referenceColumn configured" in {
    an[InvalidParameterException] should be thrownBy new DeltaReplicator(Table("cel", List("id", "eventtype"), "xivo", None))
  }

  it should "count delta replication activity and log if threshold is reached" in {
    val table = Table("cel", List("id", "uniqueid", "linkedid", "the_ref"), "xivo", Some("the_ref"))
    val lastId = 15
    val newLastId = 25
    val mockLogger = mockito.mock[Logger]
    val replicator = new DeltaReplicator(table) {
      override val logger: Logger = mockLogger
    }

    replicator.logReplicateActivity(lastId, newLastId)
    replicator.logCounter shouldEqual 10
    verify(mockLogger, never()).info(anyString())

    replicator.logReplicateActivity(lastId, 250)
    verify(mockLogger).info(anyString())

    replicator.logCounter shouldEqual 0
  }

  it should "remove referenceColumn from delta replication if is generated server side" in {
    val table = Table("cel", List("useless_uuid", "uniqueid", "linkedid", "the_ref"), "xivo", Some("the_ref"), Some(true))
    val lastId = 1000
    val maxId = 1100
    val replicator = new DeltaReplicator(table)
    replicator.lastId = lastId

    replicator.createSelectStatement(maxId) shouldEqual
      s"""SELECT "useless_uuid", "uniqueid", "linkedid" FROM cel WHERE "the_ref" > $lastId AND "the_ref" <= $maxId ORDER BY "the_ref" ASC"""
  }

}
