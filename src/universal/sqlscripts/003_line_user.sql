CREATE TABLE userfeatures
(
  id serial NOT NULL,
  firstname character varying(128) NOT NULL DEFAULT ''::character varying,
  lastname character varying(128) NOT NULL DEFAULT ''::character varying,
  CONSTRAINT userfeatures_pkey PRIMARY KEY (id)
);

-- Table: linefeatures

CREATE TABLE linefeatures
(
  id serial NOT NULL,
  name character varying(128),
  "number" character varying(40),
  context character varying(39) NOT NULL,
  provisioningid integer NOT NULL,
  CONSTRAINT linefeatures_pkey PRIMARY KEY (id),
  CONSTRAINT linefeatures_name_key UNIQUE (name)
);

-- Table: user_line

CREATE TABLE user_line
(
  id serial NOT NULL,
  user_id integer,
  line_id integer NOT NULL,
  extension_id integer,
  CONSTRAINT user_line_pkey PRIMARY KEY (id, line_id)
);

GRANT SELECT ON ALL TABLES IN SCHEMA public TO stats;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public TO stats;
