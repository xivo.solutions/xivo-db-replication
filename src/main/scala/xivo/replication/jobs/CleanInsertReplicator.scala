package xivo.replication.jobs

import java.sql.Connection

import org.postgresql.util.PGobject
import xivo.replication.sqlUtils.{Table, Replicator}

class CleanInsertReplicator(table: Table) extends Replicator(table: Table) {
  def replicate(reportingConnection: Connection, sourceConnection: Connection) = {
    logger.info(s"Cleaning the table ${table.name}.")
    reportingConnection.prepareStatement(s"DELETE FROM ${table.name}").executeUpdate()
    logger.info(s"Starting replication of table ${table.name}.")
    logger.debug(s"The replicated columns are ${table.columns}.")
    val selectStmt = sourceConnection.prepareStatement(createSelectStatement())
    selectStmt.setFetchSize(10000)
    val insertStmt = reportingConnection.prepareStatement(createInsertStatement())
    val rs = selectStmt.executeQuery()
    while (rs.next()) {
      for (i <- 1 to table.columns.size) {
        rs.getObject(i) match {
          case o: PGobject => insertStmt.setObject(i, o.getValue)
          case o: Any => insertStmt.setObject(i, o)
          case null => insertStmt.setObject(i, null)
        }
      }
      insertStmt.executeUpdate()
    }
    rs.close()
    logger.info(s"Replication for table ${table.name} finished.")
  }
}
