CREATE TABLE preferred_callback_period (
    uuid UUID PRIMARY KEY,
    name VARCHAR(128) NOT NULL DEFAULT '',
    period_start TIME WITHOUT TIME ZONE NOT NULL,
    period_end TIME WITHOUT TIME ZONE NOT NULL,
    "default" BOOLEAN DEFAULT FALSE
);

CREATE TABLE callback_list (
    uuid UUID PRIMARY KEY,
    name VARCHAR(128) NOT NULL,
    queue_id INTEGER NOT NULL
);

CREATE TABLE callback_request (
    uuid UUID PRIMARY KEY,
    list_uuid UUID  NOT NULL,
    phone_number VARCHAR(40),
    mobile_phone_number VARCHAR(40),
    firstname VARCHAR(128),
    lastname VARCHAR(128),
    company VARCHAR(128),
    description TEXT,
    agent_id INTEGER,
    clotured BOOLEAN DEFAULT FALSE,
    preferred_callback_period_uuid UUID,
    due_date DATE NOT NULL,
    reference_number INTEGER NOT NULL
);