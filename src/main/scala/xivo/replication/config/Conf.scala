package xivo.replication.config

import com.typesafe.config._
import net.ceedubs.ficus.Ficus._
import scala.collection.JavaConversions._

trait ReplicationConf {
  def isMds: Boolean
  def isHaSlave: Boolean
}

object Conf {
  case class Database(name: String, address: String, port: Int, user: String, password: String, disabled: Boolean)

  val appConf: Config = ConfigFactory.load()

  object Reporting {
    val database = databaseFromConfig(appConf.getConfig("reportingDatabase"))
  }

  def databaseFromConfig(config: Config) = Database(
      name = config.getString("database.name"),
      address = config.getString("address"),
      port = config.getInt("port"),
      user = config.getString("database.user"),
      password = config.getString("database.password"),
      disabled = if (config.hasPath(s"disable")) { config.getBoolean(s"disable") } else { false }
    )

  object SourceDatabases {
    val databases: Map[String, Database] = appConf.getConfig("sourceDatabases").root().entrySet()
      .map(entry => entry.getKey -> databaseFromConfig(appConf.getConfig(s"sourceDatabases.${entry.getKey}")))
      .toMap
  }

  object Replication extends ReplicationConf {
    val property: String = if (isMds || isHaSlave) {
      "mds_replication"
    } else {
      "replication"
    }

    val tables = appConf.getStringList(s"$property.tables")

    def isMds = appConf.getBoolean("xds.mds")
    def isHaSlave = appConf.getBoolean("ha.slave")
    def getColumns(table: String) = appConf.getStringList(s"$property.$table.columns")
    def getReplicationType(table: String) = appConf.getString(s"$property.$table.replicationType")
    def getDeltaLogThreshold() = appConf.getInt("replication.log.threshold")
    def getDatabase(table: String) = appConf.getString(s"$property.$table.sourceDatabase")
    def getReferenceColumn(table: String): Option[String] = {
      appConf.as[Option[String]](s"$property.$table.referenceColumn")
    }
    def isReferenceServerGenerated(table: String): Option[Boolean] = {
      appConf.as[Option[Boolean]](s"$property.$table.referenceServerGenerated")
    }
  }

}
