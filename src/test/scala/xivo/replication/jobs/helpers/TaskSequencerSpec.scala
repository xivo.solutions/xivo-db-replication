package xivo.replication.jobs.helpers

import akka.actor._
import akka.testkit.{ImplicitSender, TestActorRef, TestKit, TestProbe}
import org.scalatest.mock.EasyMockSugar
import org.scalatest.{Matchers, WordSpecLike}
import xivo.replication.jobs.helpers.TaskSequencer.{Reschedule, DoJob}

class TaskSequencerSpec
  extends TestKit(ActorSystem("TaskSeq")) with WordSpecLike with ImplicitSender with Matchers with EasyMockSugar {

  class Helper {
    val task = TestProbe()
    def actor() = {
      val a = TestActorRef(new TaskSequencer(task.ref))
      (a, a.underlyingActor)
    }
  }

  "A TaskSequencer" should {
    "transfer Start to the task and set the isTaskRunning flag" in new Helper {
      val (ref, a) = actor()
      ref ! DoJob

      task.expectMsg(DoJob)
      ref.underlyingActor.isTaskRunning shouldBe true
    }

    "unset the isTaskRunning flag when Done received" in new Helper {
      val (ref, a) = actor()
      ref.underlyingActor.isTaskRunning = true
      ref ! Reschedule

      ref.underlyingActor.isTaskRunning shouldBe false
    }

    "do not transfer Start to the task if isTaskRunning is set" in new Helper {
      val (ref, a) = actor()
      ref.underlyingActor.isTaskRunning = true
      ref ! DoJob

      task.expectNoMsg()
    }
  }
}
