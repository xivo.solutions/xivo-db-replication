CREATE ROLE test WITH PASSWORD 'test';
ALTER ROLE test WITH LOGIN;
CREATE DATABASE testdata WITH OWNER test;

\c testdata
--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: queue_category; Type: TYPE; Schema: public; Owner: test
--

CREATE TYPE queue_category AS ENUM (
    'group',
    'queue'
);


ALTER TYPE public.queue_category OWNER TO test;

--
-- Name: queuemember_usertype; Type: TYPE; Schema: public; Owner: test
--

CREATE TYPE queuemember_usertype AS ENUM (
    'agent',
    'user'
);


ALTER TYPE public.queuemember_usertype OWNER TO test;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: agentfeatures; Type: TABLE; Schema: public; Owner: test; Tablespace: 
--

CREATE TABLE agentfeatures (
    id integer NOT NULL,
    numgroup integer NOT NULL,
    firstname character varying(128) DEFAULT ''::character varying NOT NULL,
    lastname character varying(128) DEFAULT ''::character varying NOT NULL,
    number character varying(40) NOT NULL,
    passwd character varying(128) NOT NULL,
    context character varying(39) NOT NULL,
    language character varying(20) NOT NULL,
    autologoff integer,
    "group" character varying(255),
    description text NOT NULL,
    preprocess_subroutine character varying(40)
);


ALTER TABLE public.agentfeatures OWNER TO test;

--
-- Name: agentfeatures_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE agentfeatures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agentfeatures_id_seq OWNER TO test;

--
-- Name: agentfeatures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE agentfeatures_id_seq OWNED BY agentfeatures.id;


--
-- Name: agentgroup; Type: TABLE; Schema: public; Owner: test; Tablespace: 
--

CREATE TABLE agentgroup (
    id integer NOT NULL,
    groupid integer NOT NULL,
    name character varying(128) DEFAULT ''::character varying NOT NULL,
    groups character varying(255) DEFAULT ''::character varying NOT NULL,
    commented integer DEFAULT 0 NOT NULL,
    deleted integer DEFAULT 0 NOT NULL,
    description text
);


ALTER TABLE public.agentgroup OWNER TO test;

--
-- Name: agentgroup_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE agentgroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agentgroup_id_seq OWNER TO test;

--
-- Name: agentgroup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE agentgroup_id_seq OWNED BY agentgroup.id;


--
-- Name: queue_log; Type: TABLE; Schema: public; Owner: test; Tablespace: 
--

CREATE TABLE queue_log (
    "time" character varying(26) DEFAULT ''::character varying NOT NULL,
    callid character varying(32) DEFAULT ''::character varying NOT NULL,
    queuename character varying(50) DEFAULT ''::character varying NOT NULL,
    agent character varying(50) DEFAULT ''::character varying NOT NULL,
    event character varying(20) DEFAULT ''::character varying NOT NULL,
    data1 character varying(30) DEFAULT ''::character varying,
    data2 character varying(30) DEFAULT ''::character varying,
    data3 character varying(30) DEFAULT ''::character varying,
    data4 character varying(30) DEFAULT ''::character varying,
    data5 character varying(30) DEFAULT ''::character varying,
    id integer NOT NULL
);


ALTER TABLE public.queue_log OWNER TO test;

--
-- Name: queuefeatures; Type: TABLE; Schema: public; Owner: test; Tablespace: 
--

CREATE TABLE queuefeatures (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    displayname character varying(128) NOT NULL,
    number character varying(40) DEFAULT ''::character varying NOT NULL,
    context character varying(39),
    data_quality integer DEFAULT 0 NOT NULL,
    hitting_callee integer DEFAULT 0 NOT NULL,
    hitting_caller integer DEFAULT 0 NOT NULL,
    retries integer DEFAULT 0 NOT NULL,
    ring integer DEFAULT 0 NOT NULL,
    transfer_user integer DEFAULT 0 NOT NULL,
    transfer_call integer DEFAULT 0 NOT NULL,
    write_caller integer DEFAULT 0 NOT NULL,
    write_calling integer DEFAULT 0 NOT NULL,
    url character varying(255) DEFAULT ''::character varying NOT NULL,
    announceoverride character varying(128) DEFAULT ''::character varying NOT NULL,
    timeout integer,
    preprocess_subroutine character varying(39),
    announce_holdtime integer DEFAULT 0 NOT NULL,
    waittime integer,
    waitratio double precision
);


ALTER TABLE public.queuefeatures OWNER TO test;

--
-- Name: queuefeatures_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE queuefeatures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.queuefeatures_id_seq OWNER TO test;

--
-- Name: queuefeatures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE queuefeatures_id_seq OWNED BY queuefeatures.id;


--
-- Name: queuemember; Type: TABLE; Schema: public; Owner: test; Tablespace: 
--

CREATE TABLE queuemember (
    queue_name character varying(128) NOT NULL,
    interface character varying(128) NOT NULL,
    penalty integer DEFAULT 0 NOT NULL,
    commented integer DEFAULT 0 NOT NULL,
    usertype queuemember_usertype NOT NULL,
    userid integer NOT NULL,
    channel character varying(25) NOT NULL,
    category queue_category NOT NULL,
    "position" integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.queuemember OWNER TO test;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY agentfeatures ALTER COLUMN id SET DEFAULT nextval('agentfeatures_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY agentgroup ALTER COLUMN id SET DEFAULT nextval('agentgroup_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY queuefeatures ALTER COLUMN id SET DEFAULT nextval('queuefeatures_id_seq'::regclass);


--
-- Data for Name: agentfeatures; Type: TABLE DATA; Schema: public; Owner: test
--

COPY agentfeatures (id, numgroup, firstname, lastname, number, passwd, context, language, autologoff, "group", description, preprocess_subroutine) FROM stdin;
2	7	Aline	Belle	1702		sales	en	\N	\N		\N
1	7	Jack	Wright	1502		market	fr	\N	\N		\N
\.


--
-- Name: agentfeatures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test
--

SELECT pg_catalog.setval('agentfeatures_id_seq', 1, false);


--
-- Data for Name: agentgroup; Type: TABLE DATA; Schema: public; Owner: test
--

COPY agentgroup (id, groupid, name, groups, commented, deleted, description) FROM stdin;
1	1	marketing		0	0	\N
7	7	sales		0	0	\N
\.


--
-- Name: agentgroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test
--

SELECT pg_catalog.setval('agentgroup_id_seq', 1, false);


--
-- Data for Name: queue_log; Type: TABLE DATA; Schema: public; Owner: test
--

COPY queue_log ("time", callid, queuename, agent, event, data1, data2, data3, data4, data5, id) FROM stdin;
2015-09-24 13:39:55.63103	1393001177.288	blue	NONE	ABANDON	2	1	12			12
2015-09-24 13:39:55.63103	1393006699.288	red	Agent/1502	CONNECT	\N	\N	\N			13
2015-09-24 13:39:55.63103	1393004455.288	NONE	Agent/1702	WRAPUPSTART	\N	\N	\N			14
\.


--
-- Data for Name: queuefeatures; Type: TABLE DATA; Schema: public; Owner: test
--

COPY queuefeatures (id, name, displayname, number, context, data_quality, hitting_callee, hitting_caller, retries, ring, transfer_user, transfer_call, write_caller, write_calling, url, announceoverride, timeout, preprocess_subroutine, announce_holdtime, waittime, waitratio) FROM stdin;
101	blue	bluequeue	3000	default	0	0	0	0	0	0	0	0	0			\N	\N	0	\N	\N
102	yellow	bluequeue	3001	default	0	0	0	0	0	0	0	0	0			\N	\N	0	\N	\N
\.


--
-- Name: queuefeatures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test
--

SELECT pg_catalog.setval('queuefeatures_id_seq', 1, false);


--
-- Data for Name: queuemember; Type: TABLE DATA; Schema: public; Owner: test
--

COPY queuemember (queue_name, interface, penalty, commented, usertype, userid, channel, category, "position") FROM stdin;
blue	Agent/1230	2	0	agent	21	Agent	queue	0
yellow	Agent/1230	7	0	agent	21	Agent	queue	0
\.


--
-- Name: agentfeatures_number_key; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY agentfeatures
    ADD CONSTRAINT agentfeatures_number_key UNIQUE (number);


--
-- Name: agentfeatures_pkey; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY agentfeatures
    ADD CONSTRAINT agentfeatures_pkey PRIMARY KEY (id);


--
-- Name: agentgroup_pkey; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY agentgroup
    ADD CONSTRAINT agentgroup_pkey PRIMARY KEY (id);


--
-- Name: queuefeatures_name_key; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY queuefeatures
    ADD CONSTRAINT queuefeatures_name_key UNIQUE (name);


--
-- Name: queuefeatures_pkey; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY queuefeatures
    ADD CONSTRAINT queuefeatures_pkey PRIMARY KEY (id);


--
-- Name: queuemember_pkey; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY queuemember
    ADD CONSTRAINT queuemember_pkey PRIMARY KEY (queue_name, interface);


--
-- Name: queuemember_queue_name_channel_usertype_userid_category_key; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY queuemember
    ADD CONSTRAINT queuemember_queue_name_channel_usertype_userid_category_key UNIQUE (queue_name, channel, usertype, userid, category);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;

--
-- PostgreSQL database dump complete
--

