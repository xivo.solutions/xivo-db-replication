package xivo.replication.sqlUtils

import java.sql._

import org.scalatest.mock.EasyMockSugar
import org.scalatest.{FlatSpec, Matchers}
import xivo.replication.config.ReplicationConf

class TableSpec  extends FlatSpec with Matchers with EasyMockSugar {

  class Helper() {
    val noMdsReplicationConf: ReplicationConf = mock[ReplicationConf]

    val withMdsReplicationConf: ReplicationConf = new ReplicationConf {
      override def isMds: Boolean = true
      override def isHaSlave: Boolean = true
    }
  }

  "A TableUtils" should "read state values from the state table" in new Helper{
    val tableUtils = new TableUtils(Table("cel", List("uniqueid", "eventtype"), "xivo"),noMdsReplicationConf)
    implicit val sourceConnection = mock[Connection]
    val request = "SELECT val from replication_state WHERE name = ?"
    val statement = mock[PreparedStatement]
    val resultSet = mock[ResultSet]

    sourceConnection.prepareStatement(request).andReturn(statement)
    statement.setString(1, "cel")
    statement.executeQuery().andReturn(resultSet)

    resultSet.next().andReturn(true)
    resultSet.getInt("val").andReturn(123456)
    statement.close()

    whenExecuting(sourceConnection, statement, resultSet) {
      tableUtils.getReferenceId() shouldBe 123456
    }
  }

  it should "return 0 if there is nothing in the state table" in new Helper{
    val tableUtils = new TableUtils(Table("cel", List("uniqueid", "eventtype"), "xivo"), noMdsReplicationConf)
    implicit val localConnection = mock[Connection]
    val request = "SELECT val from replication_state WHERE name = ?"
    val statement = mock[PreparedStatement]
    val resultSet = mock[ResultSet]

    localConnection.prepareStatement(request).andReturn(statement)
    statement.setString(1, "cel")
    statement.executeQuery().andReturn(resultSet)

    resultSet.next().andReturn(false)
    statement.close()

    whenExecuting(localConnection, statement, resultSet) {
      tableUtils.getReferenceId() shouldBe 0
    }
  }

  it should "write the new state in the state table" in new Helper{
    val tableUtils = new TableUtils(Table("cel", List("uniqueid", "eventtype"), "xivo"),noMdsReplicationConf)
    implicit val localConnection = mock[Connection]
    val request = "UPDATE replication_state SET val = ? WHERE name = ?"
    val statement = mock[PreparedStatement]

    localConnection.prepareStatement(request).andReturn(statement)
    statement.setInt(1, 12345)
    statement.setString(2, "cel")
    statement.executeUpdate().andReturn(1)
    statement.close()

    whenExecuting(localConnection, statement) {
      tableUtils.setReferenceId(12345)
    }
  }

  it should "insert a new tuple in the table if it is empty" in new Helper{
    val tableUtils = new TableUtils(Table("cel", List("uniqueid", "eventtype"), "xivo"), noMdsReplicationConf)
    implicit val localConnection = mock[Connection]
    val updateRq = "UPDATE replication_state SET val = ? WHERE name = ?"
    val insertRq = "INSERT INTO replication_state(name, ref, val) VALUES (?, ?, ?)"
    val stmtUpdate = mock[PreparedStatement]
    val stmtInsert = mock[PreparedStatement]

    localConnection.prepareStatement(updateRq).andReturn(stmtUpdate)
    stmtUpdate.setInt(1, 12345)
    stmtUpdate.setString(2, "cel")
    stmtUpdate.executeUpdate().andReturn(0)
    stmtUpdate.close()

    localConnection.prepareStatement(insertRq).andReturn(stmtInsert)
    stmtInsert.setString(1, "cel")
    stmtInsert.setString(2, "id")
    stmtInsert.setInt(3, 12345)
    stmtInsert.executeUpdate().andReturn(1)
    stmtInsert.close()

    whenExecuting(localConnection, stmtUpdate, stmtInsert) {
      tableUtils.setReferenceId(12345)
    }
  }
}
