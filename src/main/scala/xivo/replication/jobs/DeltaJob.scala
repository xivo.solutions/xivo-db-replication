package xivo.replication.jobs

import java.sql.Connection

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import xivo.replication.jobs.helpers.{ScheduledJob, TaskSequencer}
import xivo.replication.jobs.helpers.TaskSequencer.{Reschedule, DoJob}
import xivo.replication.sqlUtils.{ConnectionProvider, Transaction}

import scala.concurrent.duration.FiniteDuration

class DeltaJob(replicators: List[DeltaReplicator], connectionProvider: ConnectionProvider, frequency: FiniteDuration,
               runnerProps: Option[ActorRef => Props] = None)
  extends Actor with ActorLogging with ScheduledJob {

  val props: Props = runnerProps.getOrElse((a: ActorRef) => Props(new TaskSequencer(a)))(self)
  val scheduler = scheduleJob(context, frequency, props)

  def receive = {
    case DoJob =>
      val reportingConnection = connectionProvider.getReportingConnection
      val sourceConnection = connectionProvider.getSourceConnection
      val maxIds = getMaxIds(sourceConnection)
      new Transaction(
        reportingConnection,
        sourceConnection,
        () => replicators.zip(maxIds).foreach(t => t._1.replicate(reportingConnection, sourceConnection, t._2))
      ).execute()
      sender ! Reschedule
  }

  def getMaxIds(c: Connection): List[Int] = {
    val rs = c.prepareStatement(maxIdsQuery).executeQuery()
    if(rs.next()) {
      (1 to replicators.length).map(i => Option(rs.getInt(i))).map(o => o.getOrElse(0)).toList
    } else {
      replicators.map(r => 0)
    }
  }

  override def postStop: Unit = {
    scheduler.cancel()
    connectionProvider.getReportingConnection.close()
    connectionProvider.getSourceConnection.close()
  }

  private val projectionClause = (1 to replicators.length).map(i => s"t$i.max").mkString(",")
  private val selectionClause = replicators.zipWithIndex.map(t =>
    s"(SELECT max(${t._1.table.referenceColumn.getOrElse("id")}) FROM ${t._1.table.name}) t${t._2 + 1}").mkString(",")
  val maxIdsQuery = s"SELECT $projectionClause FROM $selectionClause"
}
