package xivo.replication.sqlUtils

import java.sql.Connection

import xivo.replication.config.ReplicationConf

case class Table(name: String, columns: List[String], database: String, referenceColumn: Option[String]=None, referenceServerGenerated: Option[Boolean]=None)

class TableUtils(val table: Table, conf: ReplicationConf) {
  val replicationTable = "replication_state"

  val selectRequest = s"SELECT val from $replicationTable WHERE name = ?"
  val updateRequest = s"UPDATE $replicationTable SET val = ? WHERE name = ?"
  val insertRequest = s"INSERT INTO $replicationTable(name, ref, val) VALUES (?, ?, ?)"

  def getReferenceId()(implicit conn: Connection): Int = {
    var res = 0
    val stmt = conn.prepareStatement(selectRequest)
    stmt.setString(1, table.name)
    val resultSet = stmt.executeQuery()
    if(resultSet.next())
      res = resultSet.getInt("val")
    stmt.close()
    res
  }

  def setReferenceId(id: Int)(implicit conn: Connection) = {
    val stmt = conn.prepareStatement(updateRequest)
    stmt.setInt(1, id)
    stmt.setString(2, table.name)
    val nbUpdates = stmt.executeUpdate()
    stmt.close()
    if(nbUpdates == 0) {
      val stmt2 = conn.prepareStatement(insertRequest)
      stmt2.setString(1, table.name)
      stmt2.setString(2, table.referenceColumn.getOrElse("id"))
      stmt2.setInt(3, id)
      stmt2.executeUpdate()
      stmt2.close()
    }
  }
}
