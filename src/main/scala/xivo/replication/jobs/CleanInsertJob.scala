package xivo.replication.jobs

import akka.actor.{Actor, ActorRef, Props}
import xivo.replication.jobs.helpers.TaskSequencer.{Reschedule, DoJob}
import xivo.replication.jobs.helpers.{ScheduledJob, TaskSequencer}
import xivo.replication.sqlUtils.{ConnectionProvider, Transaction}

import scala.concurrent.duration.FiniteDuration

class CleanInsertJob(replicators: List[CleanInsertReplicator], connectionProvider: ConnectionProvider,
                     frequency: FiniteDuration, runnerProps: Option[ActorRef => Props] = None)
  extends Actor with ScheduledJob {

  val props: Props = runnerProps.getOrElse((a: ActorRef) => Props(new TaskSequencer(a)))(self)
  val scheduler = scheduleJob(context, frequency, props)

  def receive = {
    case DoJob =>
      val reportingConnection = connectionProvider.getReportingConnection()
      val sourceConnection = connectionProvider.getSourceConnection()
      replicators.foreach(r => new Transaction(reportingConnection, sourceConnection, () => r.replicate(reportingConnection, sourceConnection)).execute())
      sender ! Reschedule
  }

  override def postStop: Unit = {
    connectionProvider.getReportingConnection().close()
    connectionProvider.getSourceConnection().close()
    scheduler.cancel()
  }

}
