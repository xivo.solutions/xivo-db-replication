package integration

import java.sql.{Connection, DriverManager}

import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import xivo.replication.sqlUtils.QueueLogAdapter

import scala.sys.process._

object QueueLogAdapterSpecConf {
  val pgImage = "xivoxc/testxdbreplication:0.3"
  val containerName = "queuelogspec"
  val exposedPort = "5444"
  val sqlDump = "src/test/resources/testdata.sql"
  val driver = "org.postgresql.Driver"
  val url = s"jdbc:postgresql://127.0.0.1:$exposedPort/testdata?loginTimeout=30&connectionTimeout=30&socketTimeout=30"
  val username = "test"
  val password = "test"
  val waitForDatabasePeriod = 70
  val dockerQueueLogId = 12
  val dockerQueueLogCount = 2

  def connect: Option[Connection] = {
    val nbTries = 5
    for(i <- (0 to nbTries)) {
      val connection = tryConnect
      if (connection.isLeft) {
        return Some(connection.left.get)
      }
      Thread.sleep(waitForDatabasePeriod/nbTries*1000)
    }
    return None
  }

  private def tryConnect: Either[Connection,Throwable] = {
    try {
      Class.forName(driver)
      return Left(DriverManager.getConnection(url, username, password))
    } catch {
      case e: Throwable =>
        return Right(e)
    }
  }

}

class QueueLogAdapterSpec extends FlatSpec with Matchers with BeforeAndAfterAll {

  import QueueLogAdapterSpecConf._
  lazy val conn: Option[Connection] = connect

  override def beforeAll = {
    checkResult(stringToProcess(s"docker pull ${pgImage}").!, s"Unable to pull ${pgImage}")
    checkResult(
      stringToProcess(s"docker run -d -p $exposedPort:5432 -v /etc/localtime:/etc/localtime:ro -e POSTGRES_PASSWORD=$password --name $containerName $pgImage").!,
      s"Unable to start container $containerName")
  }

  override def afterAll = {
    conn.map(conn => {
      conn.close()
    })
    checkResult(
      stringToProcess(s"docker stop $containerName").!,
      s"Unable to stop container $containerName")
    checkResult(
      stringToProcess(s"docker rm $containerName").!,
      s"Unable to remove container $containerName")
  }

  private def checkResult(value: Int, message: String) = {
    if (value != 0) {
      fail(message)
    }
  }

  "queuelog" should "be able to be retreived" in {
      val queueLog = new QueueLogAdapter()
      queueLog.getAll(conn.get, dockerQueueLogId).size shouldEqual dockerQueueLogCount
  }

}
