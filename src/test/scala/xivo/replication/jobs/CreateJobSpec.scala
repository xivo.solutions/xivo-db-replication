package xivo.replication.jobs

import org.scalatest.mock.MockitoSugar
import org.scalatest.{Matchers, WordSpecLike}
import xivo.replication.config.Conf
import xivo.replication.sqlUtils.ReplicatorBuilder
import xivo.replication.Main._

import scala.concurrent.duration._

class CreateJobSpec extends WordSpecLike with Matchers with MockitoSugar {

  "A CreateJob" should {
    "get disable status for database xivo" in {
      Conf.SourceDatabases.databases("xivo").disabled shouldEqual false
    }

    "not create a job if database is disabled" in {
      val builder: ReplicatorBuilder = new ReplicatorBuilder()

      for((db,replicators) <- builder.getDeltaReplicators) {
        if (db == "xivo") {
          createJob[DeltaReplicator](db, replicators, new DeltaJob(_, _, 100.milliseconds)).isEmpty shouldBe (false)
        }
      }
    }
  }
}
