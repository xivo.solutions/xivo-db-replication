package xivo.replication.jobs.helpers

import akka.actor.{Actor, ActorLogging, ActorRef}
import xivo.replication.jobs.helpers.TaskSequencer.{DoJob, Reschedule}

object TaskSequencer {
  sealed trait TaskCommand
  case object DoJob extends TaskCommand
  case object Reschedule extends TaskCommand
}

class TaskSequencer(task: ActorRef) extends Actor with ActorLogging {
  var isTaskRunning = false

  def receive: Receive = {
    case DoJob if !isTaskRunning => task ! DoJob
      isTaskRunning = true

    case Reschedule => isTaskRunning = false
  }

}
