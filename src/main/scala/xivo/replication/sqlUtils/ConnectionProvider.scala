package xivo.replication.sqlUtils

import java.sql.Connection

import org.slf4j.LoggerFactory

class ConnectionProvider(reportingFactory: ConnectionFactory, sourceFactory: ConnectionFactory) {

  val logger = LoggerFactory.getLogger(getClass)

  var reportingConnection = reportingFactory.getConnection
  var sourceConnection = sourceFactory.getConnection

  def getReportingConnection(): Connection = {
    reportingConnection = getAndReconnect(reportingConnection, reportingFactory)
    reportingConnection
  }
  def getSourceConnection(): Connection = {
    sourceConnection = getAndReconnect(sourceConnection, sourceFactory)
    sourceConnection
  }

  private def getAndReconnect(connection: Connection, factory: ConnectionFactory): Connection = {
    var res = connection
    while(!res.isValid(1)) {
      try {
        logger.warn(s"Disconnected from server ${factory.uri}, trying to reconnect")
        res = factory.getConnection
      } catch {
        case e: Exception => logger.warn(s"Cannot reconnect to server ${factory.uri}")
          logger.info("Trying to reconnect in 1 second")
          Thread.sleep(1000)
      }
    }
    res
  }
}
