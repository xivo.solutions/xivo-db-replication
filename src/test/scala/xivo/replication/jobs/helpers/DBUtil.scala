package xivo.replication.jobs.helpers

import java.sql.{Connection, DriverManager}
import java.util.Properties

import org.dbunit.database.DatabaseConnection
import org.dbunit.dataset.xml.{FlatXmlDataSet, FlatXmlProducer}
import org.dbunit.operation.DatabaseOperation
import org.xml.sax.InputSource

object DBUtil {

  val HOST = "localhost"
  val DB_NAME = "asterisktest"
  val USER = "asterisk"
  val PASSWORD = "asterisk"
  val PORT = "5432"
  val connection = getConnection
  var setupDone = false

  def getConnection: Connection = {
    Class.forName("org.postgresql.Driver")
    val uri = "jdbc:postgresql://" + HOST + ":" + PORT + "/" + DB_NAME
    val props = new Properties()
    props.setProperty("user", USER)
    props.setProperty("password", PASSWORD)
    DriverManager.getConnection(uri, props)
  }

  def setupDB(filename: String, connection: Connection) = {
    if(!setupDone) {
      val dataset = new FlatXmlDataSet(new FlatXmlProducer(new InputSource(getClass.getClassLoader.getResourceAsStream(filename))))
      for (table <- dataset.getTableNames) {
        val createTable = scala.io.Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(s"$table.sql")).mkString
        connection.prepareStatement(createTable).execute()
      }
      val dbunitConnection: DatabaseConnection = new DatabaseConnection(connection)
      DatabaseOperation.CLEAN_INSERT.execute(dbunitConnection, dataset)
      setupDone = true
    }
  }

  def cleanTables(tables: List[String])(implicit connection: Connection) {
    val st = connection.createStatement()
    st.executeUpdate(s"TRUNCATE ${tables.mkString(",")} CASCADE")
  }

}
