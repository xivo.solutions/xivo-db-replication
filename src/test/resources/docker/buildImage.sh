#!/bin/bash
VERSION_TAG=0.3

docker build --tag="xivoxc/testxdbreplication:${VERSION_TAG}" --file="Dockerfile" .
docker build --tag="xivoxc/testxdbreplication:latest" --file="Dockerfile" .
docker push xivoxc/testxdbreplication:${VERSION_TAG}
