package xivo.replication

import akka.actor._
import org.slf4j.{Logger, LoggerFactory}
import xivo.replication.config.Conf
import xivo.replication.config.Conf.Database
import xivo.replication.jobs._
import xivo.replication.sqlUtils._

import scala.collection.JavaConverters._
import scala.concurrent.duration._



object Main {

  type JobFactory[T <: Replicator] = (List[T], ConnectionProvider) => Actor

  val logger: Logger = LoggerFactory.getLogger(getClass)
  val reportingFactory = new ConnectionFactory(Conf.Reporting.database)
  val system = ActorSystem()


  def databaseReachable(db: Database): Boolean = {
    try {
      val c = new ConnectionFactory(db).getConnection
      if(c.isValid(3)) {
        c.close()
        true
      } else {
        logger.warn(s"Connection to database $db timed out")
        c.close()
        false
      }
    } catch {
      case e: Exception => logger.warn(s"Error when trying to connect to database $db", e)
        false
    }
  }

  def createJob[T <: Replicator](db: String, replicators: List[T], jobFactory: JobFactory[T]): Option[ActorRef] = {
    Conf.SourceDatabases.databases.get(db) match {
      case None =>
        logger.warn(s"Database $db not configured, skipping job creation for tables ${replicators.map(_.table)}")
        None
      case Some(database) if database.disabled =>
        logger.info(s"Database $db is disabled.")
        None
      case Some(database) if databaseReachable(database) =>
        val sourceFactory = new ConnectionFactory(database)
        Some(system.actorOf(Props(jobFactory(replicators, new ConnectionProvider(reportingFactory, sourceFactory)))))
      case Some(database) if !databaseReachable(database) =>
        logger.warn(s"Cannot connect to $db, skipping job creation for tables ${replicators.map(_.table)}")
        None
    }
  }

  def logConfiguration(): Unit = {
    logger.info("**** Configuration used ****")
    Conf.appConf.root().entrySet().iterator().asScala.foreach(
      entry => logger.info(s"${entry.getKey} : ${entry.getValue}")
    )
    logger.info("****************************")
  }

  def main(args: Array[String]): Unit = {
    logger.info("Starting xivo-db-replication")
    logConfiguration()

    logger.info("Database replication daemon starting.")
    val builder = new ReplicatorBuilder()

    for((db,replicators) <- builder.getDeltaReplicators) {
      createJob[DeltaReplicator](db, replicators, new DeltaJob(_, _, 100 milliseconds))
    }

    for((db,replicators) <- builder.getCleanInsertReplicators) {
      createJob[CleanInsertReplicator](db, replicators, new CleanInsertJob(_, _, 5 minutes))
    }

    sys.addShutdownHook({
      logger.info("Shutdown signal received, exiting...")
      logger.info("Jobs canceled")
      system.shutdown()
      logger.info("Actor system shut down")
    })
  }
}
