package xivo.replication.jobs

import java.sql.Connection

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.testkit.{TestActorRef, TestKit}
import org.scalatest.mock.MockitoSugar
import org.scalatest.{Matchers, WordSpecLike}
import xivo.replication.sqlUtils.{ConnectionProvider, Table}
import xivo.replication.jobs.helpers.{DBUtil}
import org.mockito.Mockito.when

import scala.concurrent.duration._

class DeltaJobSpec extends TestKit(ActorSystem("DeltaJobSpec")) with Matchers with MockitoSugar with WordSpecLike {

  class Helper() {
    val r1 = new DeltaReplicator(Table("cel", List("id", "c1", "c2"), "xivo", Some("id")))
    val r2 = new DeltaReplicator(Table("queue_log", List("id", "c1", "c2"), "xivo", Some("id")))
    val provider = mock[ConnectionProvider]
    val cnxMock = mock[Connection]
    when(provider.getReportingConnection()).thenReturn(cnxMock)
    when(provider.getSourceConnection()).thenReturn(cnxMock)
  }

  "A DeltaJob" should {
    "create a request retrieving the maximum id for each table" in {
      val r1 = new DeltaReplicator(Table("cel", List("id", "c1", "c2"), "xivo", Some("id")))
      val r2 = new DeltaReplicator(Table("queue_log", List("id", "c1", "c2"), "xivo", Some("reference_number")))
      val provider = mock[ConnectionProvider]
      val job = TestActorRef(new DeltaJob(List(r1, r2), provider, 10 minutes,
        Some((any: ActorRef) => Props(new Actor{ def receive: Receive = {case _ => }}))))

      job.underlyingActor.maxIdsQuery shouldEqual "SELECT t1.max,t2.max FROM (SELECT max(id) FROM cel) t1,(SELECT max(reference_number) FROM queue_log) t2"
    }

    "actually retrieve the maximum id for each table" in new Helper() {
      val job = TestActorRef(new DeltaJob(List(r1, r2), provider, 10 minutes,
        Some((any: ActorRef) => Props(new Actor{ def receive: Receive = {case _ => }}))))

      val connection = DBUtil.getConnection
      DBUtil.setupDB("database.xml", connection)

      insertId("queue_log", 12, connection)
      insertId("queue_log", 13, connection)
      insertId("cel", 21, connection)
      insertId("cel", 22, connection)
      job.underlyingActor.getMaxIds(connection) shouldEqual List(22, 13)
    }

    "schedule processing" in new Helper() {
      val job = TestActorRef(new DeltaJob(List(r1, r2), provider, 10 minutes,
        Some((any: ActorRef) => Props(new Actor{ def receive: Receive = {case _ => }}))))

      job.underlyingActor.scheduler.isCancelled shouldBe(false)
    }

    "cancel scheduler on stop" in new Helper() {
      val job = TestActorRef(new DeltaJob(List(r1, r2), provider, 10 minutes,
        Some((any: ActorRef) => Props(new Actor{ def receive: Receive = {case _ => }}))))
      val scheduler = job.underlyingActor.scheduler
      system.stop(job)
      awaitCond(scheduler.isCancelled == true,100 milliseconds)
    }
  }

  private def insertId(table: String, id: Int, c: Connection) = c.prepareStatement(s"INSERT INTO $table(id) VALUES ($id)").executeUpdate()

}
