package xivo.replication.sqlUtils

import java.sql.Connection

import org.slf4j.LoggerFactory

class Transaction(reportingConnection: Connection, sourceConnection: Connection, f: () => Unit) {
  val logger = LoggerFactory.getLogger(getClass)

  def execute(): Unit = {
    reportingConnection.setAutoCommit(false)
    sourceConnection.setAutoCommit(false)
    try {
      f()
      reportingConnection.commit()
      sourceConnection.commit()
    } catch {
      case e: Exception => logger.error("An error occurred when executing a transaction", e)
        reportingConnection.rollback()
        sourceConnection.rollback()
    }
    sourceConnection.setAutoCommit(true)
    reportingConnection.setAutoCommit(true)
  }
}
