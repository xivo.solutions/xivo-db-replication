package xivo.replication.sqlUtils

import org.slf4j.LoggerFactory
import xivo.replication.config.Conf
import xivo.replication.jobs.{CleanInsertReplicator, DeltaReplicator}

import scala.collection.JavaConversions._

class ReplicatorBuilder {

  val logger = LoggerFactory.getLogger(getClass)

  def getDeltaReplicators: Map[String, List[DeltaReplicator]] = Conf.Replication.tables
    .filter(Conf.Replication.getReplicationType(_) == "DELTA")
    .map(buildTable)
    .groupBy(_.database)
    .map(t => (t._1, t._2.map(new DeltaReplicator(_)).toList))

  def getCleanInsertReplicators: Map[String, List[CleanInsertReplicator]] = Conf.Replication.tables
    .filter(Conf.Replication.getReplicationType(_) == "CLEAN_INSERT")
    .map(buildTable)
    .groupBy(_.database)
    .map(t => (t._1, t._2.map(new CleanInsertReplicator(_)).toList))

  private def buildTable(table: String): Table = Table(table, Conf.Replication.getColumns(table).toList, Conf.Replication.getDatabase(table),
      Conf.Replication.getReferenceColumn(table), Conf.Replication.isReferenceServerGenerated(table))
}
