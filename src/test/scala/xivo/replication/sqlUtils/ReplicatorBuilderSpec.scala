package xivo.replication.sqlUtils

import org.scalatest.mock.EasyMockSugar
import org.scalatest.{FlatSpec, Matchers}
import xivo.replication.jobs.{CleanInsertReplicator, DeltaReplicator}

class ReplicatorBuilderSpec extends FlatSpec with Matchers with EasyMockSugar {

  "A ReplicatorBuilder" should "group Delta Replicators by database" in {
    val builder = new ReplicatorBuilder()

    builder.getDeltaReplicators shouldEqual Map(
      "xivo" -> List(
        new DeltaReplicator(Table("queuefeatures", List("id", "name", "displayname"), "xivo", Some("id"))),
        new DeltaReplicator(Table("callback_request", List("uuid", "firstname", "reference_number"), "xivo", Some("reference_number"))))
    )
  }

  it should "group CleanInsert Replicators by database" in {
    val builder = new ReplicatorBuilder()

    builder.getCleanInsertReplicators shouldEqual Map(
      "xivo" -> List(
        new CleanInsertReplicator(Table("agentfeatures", List("id", "number", "firstname", "lastname"), "xivo")),
        new CleanInsertReplicator(Table("callback_list", List("uuid", "name"), "xivo")))
    )
  }

}
