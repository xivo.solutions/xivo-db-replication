package xivo.replication.jobs.helpers

import akka.actor.{Actor, ActorContext, ActorRef, Cancellable, Props}

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration._
import xivo.replication.jobs.helpers.TaskSequencer.DoJob

import scala.concurrent.ExecutionContext.Implicits.global

trait ScheduledJob {
  def scheduleJob(context: ActorContext, frequency: FiniteDuration, props: Props): Cancellable = {
    val runner = context.actorOf(props)
    context.system.scheduler.schedule(0 seconds, frequency, runner, DoJob)
  }

}
