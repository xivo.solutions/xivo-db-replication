CREATE TABLE public.labels
(
    id integer NOT NULL ,
    display_name character varying(128)  NOT NULL,
    description text
);

CREATE TABLE public.userlabels
(
    id integer NOT NULL,
    user_id integer,
    label_id integer
);
