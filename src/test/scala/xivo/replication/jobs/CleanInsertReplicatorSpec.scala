package xivo.replication.jobs

import java.sql.{Connection, PreparedStatement, ResultSet}

import org.easymock.EasyMock
import org.postgresql.util.PGobject
import org.scalatest.mock.EasyMockSugar
import org.scalatest.{FlatSpec, Matchers}
import xivo.replication.sqlUtils.Table

class CleanInsertReplicatorSpec extends FlatSpec with Matchers with EasyMockSugar {

  "A CleanInsertReplicator" should "truncate and replicate the data" in {
    val table = Table("cel", List("id", "uniqueid", "linkedid", "nullablecol"), "xivo")
    val local = niceMock[Connection]
    val remote = niceMock[Connection]
    val replicator = new CleanInsertReplicator(table)
    val deleteStmt = mock[PreparedStatement]
    val selectStmt = mock[PreparedStatement]
    val insertStmt = mock[PreparedStatement]
    val rs = mock[ResultSet]
    val pgObj = mock[PGobject]

    local.prepareStatement("DELETE FROM cel").andReturn(deleteStmt)
    deleteStmt.executeUpdate().andReturn(1)
    remote.prepareStatement(EasyMock.anyObject(classOf[String])).andReturn(selectStmt)
    selectStmt.setFetchSize(10000)
    local.prepareStatement(EasyMock.anyObject(classOf[String])).andReturn(insertStmt)
    selectStmt.executeQuery().andReturn(rs)
    rs.next().andReturn(true)
    rs.getObject(1).andReturn(12.asInstanceOf[AnyRef])
    insertStmt.setObject(1, 12)
    rs.getObject(2).andReturn("b")
    insertStmt.setObject(2, "b")
    rs.getObject(3).andReturn(pgObj)
    pgObj.getValue.andReturn("c")
    insertStmt.setObject(3, "c")
    rs.getObject(4).andReturn(null)
    insertStmt.setObject(4, null)
    insertStmt.executeUpdate().andReturn(1)
    rs.next().andReturn(false)
    rs.close()

    whenExecuting(local, remote, deleteStmt, selectStmt, insertStmt, rs, pgObj) {
      replicator.replicate(local, remote)
    }
  }
}
