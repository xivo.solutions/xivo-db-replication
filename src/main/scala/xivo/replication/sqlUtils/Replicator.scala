package xivo.replication.sqlUtils

import org.slf4j.LoggerFactory

abstract class Replicator(val table: Table) {

  val logger = LoggerFactory.getLogger(getClass)

  def getColumns(): List[String] = {
    table.referenceServerGenerated match {
      case Some(true) => table.columns.filterNot(table.referenceColumn.contains)
      case _ => table.columns
    }
  }

  def createSelectStatement(): String = {
    val columns: List[String] = getColumns()

    var query = "SELECT "
    for(column <- columns) {
      query += s""""$column", """
    }
    query = query.substring(0, query.length - 2)
    query += s" FROM ${table.name}"
    query
  }

  def createInsertStatement(): String = {
    val columns: List[String] = getColumns()

    var query = s"INSERT INTO ${table.name} ("
    var valuesPart = ""
    for(column <- columns) {
      query += s""""$column", """
      valuesPart += "?, "
    }
    query = query.substring(0, query.length - 2)
    valuesPart = valuesPart.substring(0, valuesPart.length - 2)
    query += s") VALUES ($valuesPart)"
    query
  }

  override def equals(o: Any): Boolean = {
    if(getClass == o.getClass) {
      return o.asInstanceOf[Replicator].table == table
    }
    false
  }
}
