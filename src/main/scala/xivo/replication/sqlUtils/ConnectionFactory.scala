package xivo.replication.sqlUtils

import java.sql.{Connection, DriverManager}
import java.util.Properties

import org.slf4j.LoggerFactory
import xivo.replication.config.Conf.Database

class ConnectionFactory(database: Database) {
  val logger = LoggerFactory.getLogger(getClass)

  val uri = "jdbc:postgresql://" + database.address + ":"+ database.port + "/" + database.name
  val props = new Properties()
  props.setProperty("user", database.user)
  props.setProperty("password", database.password)
  props.setProperty("stringtype", "unspecified")

  logger.info(s"building connection to $uri for user ${database.user}")

  def getConnection: Connection = {
    Class.forName("org.postgresql.Driver")
    val connection = DriverManager.getConnection(uri, props)
    logger.info(s"Connection successful to ${database.name} with user ${database.user}")
    connection
  }

}
