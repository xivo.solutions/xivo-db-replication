package xivo.replication.sqlUtils

import java.sql.Connection

import org.scalatest.mock.EasyMockSugar
import org.scalatest.{FlatSpec, Matchers}

class TransactionSpec extends FlatSpec with Matchers with EasyMockSugar {

  "A Transaction" should "execute query in a transaction" in {
    val reportingConnection = mock[Connection]
    val sourceConnection = mock[Connection]
    val mockFunction = mock[() => Unit]

    reportingConnection.setAutoCommit(false)
    sourceConnection.setAutoCommit(false)
    mockFunction.apply()
    sourceConnection.commit()
    sourceConnection.setAutoCommit(true)
    reportingConnection.commit()
    reportingConnection.setAutoCommit(true)

    whenExecuting(reportingConnection, sourceConnection, mockFunction) {
      new Transaction(reportingConnection, sourceConnection, mockFunction).execute()
    }
  }

  it should "rollback if an exception is raised" in {
    class MyException extends Exception() {}
    val reportingConnection = mock[Connection]
    val sourceConnection = mock[Connection]
    def function(): Unit = throw new MyException()

    reportingConnection.setAutoCommit(false)
    sourceConnection.setAutoCommit(false)
    sourceConnection.rollback()
    sourceConnection.setAutoCommit(true)
    reportingConnection.rollback()
    reportingConnection.setAutoCommit(true)

    whenExecuting(reportingConnection, sourceConnection) {
      new Transaction(reportingConnection, sourceConnection, function).execute()
    }
  }
}
