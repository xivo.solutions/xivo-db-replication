#!/bin/bash

DATABASE_NAME="testdata"
DB_DUMP_LOCATION="/tmp/psql_data/testdata.sql"

echo "*** CREATING DATABASE ***"
psql -f "$DB_DUMP_LOCATION";
echo "*** DATABASE CREATED! ***"
