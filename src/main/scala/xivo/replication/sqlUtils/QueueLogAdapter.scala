package xivo.replication.sqlUtils

import java.util.Date
import anorm._
import anorm.SqlParser.get
import java.sql.Connection
import org.joda.time.format.DateTimeFormat
import org.slf4j.LoggerFactory

case class QueueLog(id: Int, queuetime: Date, callid: String,
                    queuename: String, queuedisplayname: Option[String], agent: String, agentnumber: Option[String], agentname: Option[String], groupname: Option[String], event: String, data1: String)

class QueueLogAdapter {
  val logger = LoggerFactory.getLogger(this.getClass)

  val query = """select
              ql.id as id,
              to_char(cast(time as timestamp), 'YYYY-MM-DD HH24:MI:SS.MS') as queuetime,
              ql.callid,
              ql.queuename,
              qf.displayname as queuedisplayname,
              ql.agent,
              af.number as agentnumber,
              af.firstname || ' ' || af.lastname as agentname,
              ql.event,
              ql.data1,
              ag.name as groupname
          from queue_log as ql
          left outer join queuefeatures as qf on (ql.queuename = qf.name)
          left outer join agentfeatures as af on (ql.agent = 'Agent/' || af.number)
          left outer join agentgroup as ag on (af.numgroup = ag.id)
          where ql.id > {id} order by ql.id desc limit 20000"""

  val simple = {
    val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss.SSS")
      get[Int]("id") ~
      get[String]("queuetime") ~
      get[String]("callid") ~
      get[String]("queuename") ~
      get[Option[String]]("queuedisplayname") ~
      get[String]("agent") ~
      get[Option[String]]("agentnumber") ~
      get[Option[String]]("agentname") ~
      get[Option[String]]("groupname") ~
      get[String]("event") ~
      get[Option[String]]("data1") map {
      case id ~ queuetime ~ callid ~ queuename ~ queuedisplayname ~ agent ~ agentnumber ~ agentname ~ groupname ~ event ~ data1 =>
        QueueLog(id, format.parseDateTime(queuetime).toDate(), callid, queuename, queuedisplayname, agent, agentnumber, agentname, groupname, event, data1.getOrElse(""))
    }
  }

  def getAll(connection: Connection, id: Int): List[QueueLog] = {
    logger.debug(s"fetching queue logs after id $id")
    implicit val conn = connection
    SQL(query).on('id -> id).as(simple *)
  }

}
