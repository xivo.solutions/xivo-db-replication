package xivo.replication.sqlUtils

import org.scalatest.mock.EasyMockSugar
import org.scalatest.{FlatSpec, Matchers}

class ReplicatorSpec extends FlatSpec with Matchers with EasyMockSugar {

  "A Replicator" should "create a SELECT statement for a Table" in {
    val table = Table("country", List("name", "population", "area"), "xivo")
    val replicator = new Replicator(table){}

    replicator.createSelectStatement() shouldEqual """SELECT "name", "population", "area" FROM country"""
  }

  it should "create an INSERT statement for a Table" in {
    val table = Table("country", List("name", "population", "area"), "xivo")
    val replicator = new Replicator(table) {}

    replicator.createInsertStatement() shouldEqual """INSERT INTO country ("name", "population", "area") VALUES (?, ?, ?)"""
  }

  it should "create an INSERT statement for a Table while removing ref column" in {
    val table1 = Table("country", List("id", "name", "population", "area"), "xivo")
    val table2 = Table("country", List("id", "name", "population", "area", "ref"), "xivo", Some("ref"), Some(true))
    val table3 = Table("country", List("id", "name", "population", "area", "ref"), "xivo", Some("ref"), Some(false))
    val replicator1 = new Replicator(table1) {}
    val replicator2 = new Replicator(table2) {}
    val replicator3 = new Replicator(table3) {}

    replicator1.createInsertStatement() shouldEqual """INSERT INTO country ("id", "name", "population", "area") VALUES (?, ?, ?, ?)"""
    replicator2.createInsertStatement() shouldEqual """INSERT INTO country ("id", "name", "population", "area") VALUES (?, ?, ?, ?)"""
    replicator3.createInsertStatement() shouldEqual """INSERT INTO country ("id", "name", "population", "area", "ref") VALUES (?, ?, ?, ?, ?)"""
  }
}
